This is a personal repo for implementing algorithms in book Algs4.

Thus, the followings are allowed:
- Small fix patches
- Free to create new branches so long as names are in lowercase and separated by a '-' or '/'