
pub mod web_exercises {
    pub fn squeeze(in_str:&str)->String {
        let mut res = String::from("");
        let chars = in_str.chars();
        let mut prev_char = 'a';
        for c in chars {
            if prev_char == ' ' && c == prev_char {

            } else {
                res.push(c);
                prev_char = c;
            }
        }

        res
    }

    pub fn mystery(mut n:usize) -> String {
        let mut res = String::new();

        while n > 0 {
            if  n % 2 == 1 { res.push_str(&res.clone()); res.push('x'); }
            //kk dup? clone?
            else {
                res.push_str(&res.clone());
            }
            n = n / 2;
        }
        res
    }

    pub fn palindrome_check(in_str:&str) -> bool {
        let (mut start, mut end) = (0, (in_str.len() as isize) -1);
        let chars:Vec<char> = in_str.chars().collect::<Vec<char>>();

        while start < end && chars[start as usize] == chars[end as usize] {
            start += 1;
            end -= 1;
        }

        start >= end
    }

    #[test]
    pub fn test_palindrome_check() {
        assert_eq!(true, palindrome_check(""), "Empty");
        assert_eq!(true, palindrome_check("aba"), "aba");
        assert_eq!(false, palindrome_check("ba"));
    }

    pub fn rm_dup(in_str:&str) -> String {
        let mut res = String::new();
        let mut chars = in_str.chars();
        if let Some(mut prev) = chars.nth(0) {
            res.push(prev);

            for c in chars {
                if c == prev {

                } else {
                    prev = c;
                    res.push(c);
                }
            }
        }

        res
    }

    #[test]
    fn test_mystery() {
        assert_eq!("xxx", mystery(3));

    }

    #[test]
    fn test_rm_dup() {
        assert_eq!("你", rm_dup("你你你"));
        assert_eq!("o", rm_dup("ooo"));
        assert_eq!("ofo", rm_dup("ofo"));
        assert_eq!("ofo", rm_dup("ooofoooo"));
        assert_eq!("", rm_dup(""));


    }

    #[test]
    fn test_sqz1() {
        assert_eq!("", squeeze(""));
        assert_eq!(" a b ", squeeze(" a b "));
        assert_eq!(" a b ", &squeeze("  a b  "));
        assert_eq!(" ", &squeeze("    "));
        assert_eq!(" 只管b ", &squeeze("  只管b  "));
    }

}
